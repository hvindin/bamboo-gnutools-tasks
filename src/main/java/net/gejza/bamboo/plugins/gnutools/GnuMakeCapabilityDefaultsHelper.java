package net.gejza.bamboo.plugins.gnutools;

import com.atlassian.bamboo.v2.build.agent.capability.*;
import net.gejza.bamboo.plugins.gnutools.tasks.GnuMakeTask;
import org.apache.commons.lang.SystemUtils;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;

/**
 * Created by gejza on 4.5.14.
 */
public class GnuMakeCapabilityDefaultsHelper extends AbstractFileCapabilityDefaultsHelper {
    private static final Logger log = Logger.getLogger(GnuMakeCapabilityDefaultsHelper.class);
    // ------------------------------------------------------------------------------------------------------- Constants

    // ------------------------------------------------------------------------------------------------- Type Properties
    // ---------------------------------------------------------------------------------------------------- Dependencies
    // ---------------------------------------------------------------------------------------------------- Constructors
    // ----------------------------------------------------------------------------------------------- Interface Methods
    @NotNull
    @Override
    protected String getExecutableName() {
        return GnuMakeTask.GNUMAKE_EXE_NAME;
    }

    @NotNull
    @Override
    protected String getCapabilityKey() {
        return GnuMakeTask.GNUMAKE_CAPABILITY_PREFIX + "." + GnuMakeTask.GNUMAKE_LABEL;
    }
    
    // -------------------------------------------------------------------------------------------------- Action Methods
    // -------------------------------------------------------------------------------------------------- Public Methods
    // ------------------------------------------------------------------------------------------------- Helper Methods
    // -------------------------------------------------------------------------------------- Basic Accessors / Mutators
}
